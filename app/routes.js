// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: '/',
      name: 'home',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/HomePage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/room/:slug',
      name: 'roomModule',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/RoomModule/reducer'),
          System.import('containers/RoomModule/sagas'),
          System.import('containers/RoomModule'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('roomModule', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/booking/:slug',
      name: 'bookingModule',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/BookingModule/reducer'),
          System.import('containers/BookingModule/sagas'),
          System.import('containers/BookingModule'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('bookingModule', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/login',
      name: 'adminModule',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/AdminModule/reducer'),
          System.import('containers/AdminModule/sagas'),
          System.import('containers/AdminModule'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('adminModule', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/result',
      name: 'roomModule',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/RoomModule/reducer'),
          System.import('containers/RoomModule/sagas'),
          System.import('containers/RoomModule'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('roomModule', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'help',
      name: 'helpPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/HelpPage/reducer'),
          System.import('containers/HelpPage/sagas'),
          System.import('containers/HelpPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('helpPage', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'about',
      name: 'aboutPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/AboutPage/reducer'),
          System.import('containers/AboutPage/sagas'),
          System.import('containers/AboutPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('aboutPage', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/admin',
      name: 'adminModule',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/AdminModule/reducer'),
          System.import('containers/AdminModule/sagas'),
          System.import('containers/AdminModule'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('adminModule', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        System.import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
