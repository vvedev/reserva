import styled from 'styled-components';

export const Logo = styled.h1`
  margin: 0;
  line-height: 1;
  color: #FFF;
  font-size: 2rem;
  font-weight: 500;
  text-transform: uppercase;
  letter-spacing: 0.25rem;
  font-family: 'Jaapokki-Enchance';
`;

export const Credit = styled.p`
  margin: 0;
  margin-top: 1rem;
  line-height: 1;
  font-size: 1rem;
  color: #FFF;
`;

export const Bottom = styled.div`
  width: 100%;
  padding: 2rem 0;
  background: #283739;
`;