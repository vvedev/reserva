/**
*
* Footer
*
*/

import React from 'react';
import { Bottom, Logo, Credit } from './styled';

class Footer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Bottom>
        <div className="row">
          <div className="small-12 columns text-center">
            <Logo>reserva</Logo>
          </div>
          <div className="small-12 columns text-center">
            <Credit>2016 © Web Development SIG.<br />Ristek Fasilkom UI</Credit>
          </div>
        </div>
      </Bottom>
    );
  }
}

export default Footer;
