/**
*
* Header
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { openMenu } from 'containers/App/actions';
import { Navbar, Logo, MenuToggle } from './styled';

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    openMenu: React.PropTypes.func,
    push: React.PropTypes.func,
  }

  render() {
    return (
      <Navbar>
        <div className="row">
          <div className="small-12 columns">
            <Logo onClick={() => this.props.push('/')}>Reserva</Logo>
            <MenuToggle onClick={this.props.openMenu}>
              <span className="flaticon-menu"></span>
            </MenuToggle>
          </div>
        </div>
      </Navbar>
    );
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    openMenu: () => dispatch(openMenu()),
    push: (url) => dispatch(push(url)),
  };
}

// Wrap the component to inject dispatch and state into it
export default connect(null, mapDispatchToProps)(Header);
