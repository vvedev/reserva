import styled from 'styled-components';

export const Logo = styled.button`
  margin: 0;
  margin-top: 0.25rem;
  line-height: 1;
  color: #0097b0;
  font-size: 1.5rem;
  font-weight: 500;
  text-transform: uppercase;
  letter-spacing: 0.25rem;
  float: left;
  font-family: 'Jaapokki-Enchance';
`;

export const Navbar = styled.div`
  width: 100%;
  padding: 1rem;
`;

export const MenuToggle = styled.button`
  float: right;
  font-size: 1.5rem;
  line-height: 1;
  color: #0097b0;
`;
