import { createSelector } from 'reselect';

/**
 * Direct selector to the adminModule state domain
 */
const selectAdminModuleDomain = () => (state) => state.get('adminModule');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AdminModule
 */

const selectAdminModule = () => createSelector(
  selectAdminModuleDomain(),
  (substate) => substate
);

export default selectAdminModule;
export {
  selectAdminModuleDomain,
};
