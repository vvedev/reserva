import expect from 'expect';
import adminModuleReducer from '../reducer';
import { fromJS } from 'immutable';

describe('adminModuleReducer', () => {
  it('returns the initial state', () => {
    expect(adminModuleReducer(undefined, {})).toEqual(fromJS({}));
  });
});
