/*
 *
 * AdminModule actions
 *
 */

import {
  DEFAULT_ACTION,
  CHANGE_TAB,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function changeTab(payload) {
  return {
    type: CHANGE_TAB,
    payload,
  };
}
