/*
 *
 * AdminModule reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  CHANGE_TAB,
} from './constants';
import roomModuleReducer from 'containers/RoomModule/reducer';
import { combineReducers } from 'redux';

const initialState = fromJS({
	tabs: 0,
});

function adminModuleReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case CHANGE_TAB:
    	return state.set('tabs', action.payload);
    default:
      return state;
  }
}

export default combineReducers({
	adminModule: adminModuleReducer,
	roomModule: roomModuleReducer,
});
