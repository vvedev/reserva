/*
 *
 * AdminModule constants
 *
 */

export const DEFAULT_ACTION = 'app/AdminModule/DEFAULT_ACTION';
export const CHANGE_TAB = 'app/AdminModule/CHANGE_TAB';
