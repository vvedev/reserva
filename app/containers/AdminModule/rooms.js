import React from 'react';
import styled from 'styled-components';
import { isEmpty } from 'lodash';

const Room = styled.div`
	min-height: calc(100vh - 3rem);
	position: relative;
	padding:0 0 2rem;
`;

const Content = styled.div`
	margin: 0 auto;
	max-width: 1200px;
`;

const Title = styled.h1`
  letter-spacing: .1rem;
  line-height: 1;
  margin: 0 0 1rem;
  font-family: 'Jaapokki-Regular';

  @media (min-width: 768px) {
    font-size: 2rem;
    padding: 0 2.5rem;
  }
`;

const Excerpt = styled.div`
	margin: 1rem;
	width: calc(100% - 2rem);
	background: #FFF;
	text-align: left;
	border: 0.2rem solid #EEE;
	border-top: none;
	border-bottom: none;
`;

const ExcerptContent = styled.div`
	padding: 2rem;
	position: relative;
`;

const Name = styled.h2`
	margin: 0;
	line-height: 1;

	@media (min-width: 768px) {
		font-size: 1.3rem;
	}
`;

const Location = styled.h5`
	margin: 0;
	line-height: 1;

	@media (min-width: 768px) {
		font-size: 1.1rem;
	}
`;

const Desc = styled.p`
	margin: 0;

	@media (min-width: 768px) {
		font-size: 1rem;
	}
`;

const Status = styled.button`
	font-style: italic;
	color: #0097b0;
	padding: 0.5rem 1rem;
	border-bottom: 0.1rem solid #0097b0;
	position: absolute;
	top: 1.5rem;
	right: 1rem;

	&:disabled {
		color: #AAA;
		border-bottom: 0.1rem solid #AAA;
	}

	@media (min-width: 768px) {
		font-size: 1.1rem;
	}
`;

const Prev = styled.img`
	width: 100%;
	height: 15rem;
	object-fit: cover;
	object-location: center;
`;

const Search = styled.div`
	padding: 1rem;
	border-top: 0.1rem solid #0097b0;

	@media screen and (min-width: 1200px) {
		padding: 1rem 0;
	}
`;

const SearchButton = styled.button`
	width: 100%;
	margin-top: 1rem;
	padding: 1rem 1rem 0.5rem;
	font-family: 'Jaapokki-Regular';
	text-transform: uppercase;
	color: #0097b0;
	font-size: 1.25rem;
	border: 0.2rem solid #0097b0;

	@media screen and (min-width: 768px) {
		margin-top: 0;
	}
`;

const AddRoom = styled.button`
	width: calc(100% - 2rem);
	padding: 1rem 1rem 0.75rem;
	font-family: 'Jaapokki-Regular';
	text-transform: uppercase;
	color: #0097b0;
	border: 0.2rem solid #0097b0;
	font-size: 1rem;
	margin: 2rem 1rem;

	&:hover,
	&:focus,
	&:active {
		color: #FFF;
		background: #0097b0;
	}
`;

const EditRoom = styled.button`
	width: 100%;
	padding: 1rem 1rem 0.75rem;
	font-family: 'Jaapokki-Regular';
	text-transform: uppercase;
	font-size: 1rem;
	color: #FFF;
	background: #fba600;
`;

const DelRoom = styled.button`
	width: 100%;
	padding: 1rem 1rem 0.75rem;
	font-family: 'Jaapokki-Regular';
	text-transform: uppercase;
	font-size: 1rem;
	color: #FFF;
	background: #DC5539;
`;

export default class Rooms extends React.Component { // eslint-disable-line react/prefer-stateless-function
	static propTypes = {
		push: React.PropTypes.func,
		rooms: React.PropTypes.object,
	};

	render() {
		let roomElement = [];

		if(!isEmpty(this.props.rooms)) {
			for (const key in this.props.rooms) {
				if(this.props.rooms.hasOwnProperty(key)) {
					roomElement.push(
						<Excerpt key={`room-${key}`} className="small-12 columns">
							<div className="row expanded">
								<div className="small-12 medium-4 columns">
									<Prev src={this.props.rooms[key].picture} alt="room picture" />
								</div>
								<ExcerptContent className="small-12 medium-8 columns">
									<Status disabled={this.props.rooms[key].status === 0} onClick={() => this.props.push(`/room/${this.props.rooms[key].name}`)}>
										{this.props.rooms[key].status === 1 ? 'Available' : 'Not Available'} <span className="flaticon-arrows" />
									</Status>
									<Name>{this.props.rooms[key].name}</Name>
									<Location>
										<span>{this.props.rooms[key].building}</span>
									</Location>
								</ExcerptContent>
							</div>
							<div className="row expanded">
								<div className="small-6 columns">
									<EditRoom>Edit</EditRoom>
								</div>
								<div className="small-6 columns">
									<DelRoom>Delete</DelRoom>
								</div>
							</div>
						</Excerpt>
					);
				}
			}
		}

		return (
			<Room>
				<Content>
					<AddRoom>Add Room</AddRoom>
					<div className="row">
						{roomElement}
					</div>
				</Content>
			</Room>
		);
	}
}
