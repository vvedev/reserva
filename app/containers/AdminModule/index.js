/*
 *
 * AdminModule
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import selectAdminModule from './selectors';
import { changeTab } from './actions';
import Bookings from './bookings';
import Rooms from './rooms';
import AddRoom from './addroom';
import styled from 'styled-components';

const Tabs = styled.button`
  width: 100%;
  border-bottom: 0.2rem solid #0097b0;
  padding: 1rem 1rem 0.75rem;
  text-align: center;
  font-family: 'Jaapokki-Regular';
  text-transform: uppercase;
  font-size: 1.25rem;

  &:disabled {
    border: 0.2rem solid #0097b0;
    border-bottom: none;
  }
`;

export class AdminModule extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    changeTab: React.PropTypes.func,
    adminModule: React.PropTypes.object,
    roomModule: React.PropTypes.object,
  };
  
  render() {
    let content = (<Bookings />);

    if(this.props.adminModule.get('tabs') === 1) {
      content = (<Rooms rooms={this.props.roomModule.get('rooms').toJS()}/>);
    }

    return (
      <div>
        <Helmet
          title="AdminModule"
          meta={[
            { name: 'description', content: 'Description of AdminModule' },
          ]}
        />
        <AddRoom />
        <div className="row">
          <div className="small-6 columns">
            <Tabs disabled={this.props.adminModule.get('tabs') === 0} onClick={() => this.props.changeTab(0)}>Bookings</Tabs>
          </div>
          <div className="small-6 columns">
            <Tabs disabled={this.props.adminModule.get('tabs') === 1} onClick={() => this.props.changeTab(1)}>Rooms</Tabs>
          </div>
        </div>
        {content}
      </div>
    );
  }
}

const mapStateToProps = selectAdminModule();

function mapDispatchToProps(dispatch) {
  return {
    changeTab: (tab) => dispatch(changeTab(tab)),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminModule);
