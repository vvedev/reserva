import expect from 'expect';
import aboutPageReducer from '../reducer';
import { fromJS } from 'immutable';

describe('aboutPageReducer', () => {
  it('returns the initial state', () => {
    expect(aboutPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
