import { Link } from 'react-router';
import styled from 'styled-components';

export default styled(Link)`
  display: inline-flex;
  margin: 1rem .5rem;
  text-decoration: none;
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  font-size: 24px;
  font-weight: bold;
  color: #41ADDD;
  width: 100%;
  text-transform: uppercase;
  
  &:active {
    background: #41ADDD;
    color: #FFF;
  }
`;
