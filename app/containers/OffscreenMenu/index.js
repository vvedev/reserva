/**
*
* OffscreenMenu
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { selectGlobal } from 'containers/App/selectors';
import { closeMenu } from 'containers/App/actions';
import { FormattedMessage } from 'react-intl';
import HeaderLink from './HeaderLink';
import messages from './messages';
import { createStructuredSelector } from 'reselect';
import styled, { keyframes } from 'styled-components';

const slideLeft = keyframes`
  from {
    left: 100%;
  }

  to {
    left: 30%;
  }
`;

const slideLeftDesktop = keyframes`
  from {
    left: 100%;
  }

  to {
    left: calc(100% - 20rem);
  }
`;

const fadeIn = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

const Offscreen = styled.div`
  display: ${(props) => props.display ? 'block' : 'none'};
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  width: 100%;
  height: 100%;
  background: linear-gradient(rgba(0,0,0,0.75), rgba(0,0,0,0.75));
  animation: ${fadeIn} 0.25s linear;
  z-index: 1;
`;

const Container = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`;

const Content = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  width: 80%;
  max-width: 20rem;
  height: 100%;
  background: #FFF;
  padding: 3rem 3rem 3rem 2rem;
  
  @media screen and (max-width: 28.6rem) {
   animation: ${slideLeft} 0.25s linear;
  }
  @media screen and (min-width: 28.6rem) {
    animation: ${slideLeftDesktop} 0.25s linear
  }
`;

const CloseButton = styled.button`
  position: absolute;
  top: -3rem;
  left: 100%;
  width: 3rem;
  height: 3rem;
  background: #DC5539;
  text-align: center;
  padding: 0;
`;

const CloseIcon = styled.span`
  line-height: 1;
  font-size: 1.5rem;
  color: #FFF;
`;

const NavLink = styled.button`
  display: inline-flex;
  margin: 1rem .5rem;
  text-decoration: none;
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  font-size: 24px;
  font-weight: bold;
  color: #41ADDD;
  width: 100%;
  text-transform: uppercase;

  &:active {
    background: #41ADDD;
    color: #FFF;
  }
`;

class OffscreenMenu extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    global: React.PropTypes.object,
    closeMenu: React.PropTypes.func,
    push: React.PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.go = this.go.bind(this);
  }

  go(url) {
    this.props.closeMenu();
    this.props.push(url);
  }
  
  render() {
    return (
      <Offscreen display={this.props.global.get('menu')}>
        <Container>
          <Content>
            <Container>
              <CloseButton onClick={this.props.closeMenu}>
                <CloseIcon className="flaticon-cancel" />
              </CloseButton>
              <NavLink onClick={() => this.go('/')}>
                <FormattedMessage {...messages.home} />
              </NavLink>
              <NavLink onClick={() => this.go('/about')}>
                <FormattedMessage {...messages.about} />
              </NavLink>
              <NavLink onClick={() => this.go('/help')}>
                <FormattedMessage {...messages.help} />
              </NavLink>
              <hr />
              <NavLink onClick={() => this.go('/booking/index')}>
                <FormattedMessage {...messages.booking} />
              </NavLink>
              <NavLink onClick={() => this.go('/logout')}>
                <FormattedMessage {...messages.logout} />
              </NavLink>
            </Container>
          </Content>
        </Container>
      </Offscreen>
    );
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    closeMenu: () => dispatch(closeMenu()),
    push: (url) => dispatch(push(url)),
  };
}

const mapStateToProps = createStructuredSelector({
  global: selectGlobal(),
});

// Wrap the component to inject dispatch and state into it
export default connect(mapStateToProps, mapDispatchToProps)(OffscreenMenu);
