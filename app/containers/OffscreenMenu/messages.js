/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  home: {
    id: 'boilerplate.components.OffscreenMenu.home',
    defaultMessage: 'Home',
  },
  booking: {
    id: 'boilerplate.components.OffscreenMenu.booking',
    defaultMessage: 'Bookings',
  },
  about: {
    id: 'boilerplate.components.OffscreenMenu.about',
    defaultMessage: 'About',
  },
  help: {
    id: 'boilerplate.components.OffscreenMenu.help',
    defaultMessage: 'Help',
  },
  logout: {
    id: 'boilerplate.components.OffscreenMenu.logout',
    defaultMessage: 'Logout',
  },
});
