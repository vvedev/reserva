import expect from 'expect';
import loginModuleReducer from '../reducer';
import { fromJS } from 'immutable';

describe('loginModuleReducer', () => {
  it('returns the initial state', () => {
    expect(loginModuleReducer(undefined, {})).toEqual(fromJS({}));
  });
});
