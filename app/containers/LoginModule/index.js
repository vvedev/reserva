/*
 *
 * LoginModule
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import selectLoginModule from './selectors';

export class LoginModule extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <Helmet
          title="LoginModule"
          meta={[
            { name: 'description', content: 'Description of LoginModule' },
          ]}
        />
      </div>
    );
  }
}

const mapStateToProps = selectLoginModule();

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginModule);
