import { createSelector } from 'reselect';

/**
 * Direct selector to the loginModule state domain
 */
const selectLoginModuleDomain = () => (state) => state.get('loginModule');

/**
 * Other specific selectors
 */


/**
 * Default selector used by LoginModule
 */

const selectLoginModule = () => createSelector(
  selectLoginModuleDomain(),
  (substate) => substate.toJS()
);

export default selectLoginModule;
export {
  selectLoginModuleDomain,
};
