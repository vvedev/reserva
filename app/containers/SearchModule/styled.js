import styled from 'styled-components';

export const DateIcon = styled.span`
  margin: 0;
  line-height: 1;
  background: #0097b0;
  color: #eee;
  font-size: 1rem;
  padding: 0.9rem 0.5rem;
  border-bottom: 0.2rem solid #0097b0;
`;

export const ClockIcon = styled.span`
  margin: 0;
  line-height: 1;
  background: #0097b0;
  color: #eee;
  font-size: 1rem;
  padding: 0.9rem 0.5rem;
  border-bottom: 0.2rem solid #0097b0;

  @media (min-width: 768px) {
    margin-left: 1rem;
  }
`;

export const ToIcon = styled.span`
  margin: 0;
  line-height: 1;
  background: #0097b0;
  color: #eee;
  font-size: 1rem;
  padding: 0.9rem 0.5rem;
  border-bottom: 0.2rem solid #0097b0;
`;