/*
 *
 * SearchModule
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import TimePicker from 'rc-time-picker';
import Moment from 'moment';
import { DateIcon, ClockIcon, ToIcon } from './styled';
import { changeQuery } from 'containers/App/actions';
import { selectGlobalJS } from 'containers/App/selectors';


export class SearchModule extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    changeInput: React.PropTypes.func,
    query: React.PropTypes.object,
  }

  render() {
    return (
      <div>
        <DateIcon className="flaticon-calendar" />
        <DatePicker
          placeholderText="Date"
          dateFormat="DD/MM/YYYY"
          selected={this.props.query.date}
          onChange={(value) => this.props.changeQuery('date', value)} />
        <ClockIcon className="flaticon-clock" />
        <TimePicker
          placeholder="Start Time"
          defaultValue={this.props.query.startHour}
          onChange={(value) => this.props.changeQuery('startHour', value)}
          showSecond={false}
        />
        <ToIcon className="flaticon-arrows" />
        <TimePicker
          placeholder="End Time"
          defaultValue={this.props.query.endHour}
          onChange={(value) => this.props.changeQuery('endHour', value)}
          showSecond={false}
        />
      </div>
    );
  }
}

const mapStateToProps = selectGlobalJS();

function mapDispatchToProps(dispatch) {
  return {
    changeQuery: (field, value) => dispatch(changeQuery(field, value)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchModule);
