import expect from 'expect';
import searchModuleReducer from '../reducer';
import { fromJS } from 'immutable';

describe('searchModuleReducer', () => {
  it('returns the initial state', () => {
    expect(searchModuleReducer(undefined, {})).toEqual(fromJS({}));
  });
});
