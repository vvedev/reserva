/*
 *
 * HelpPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import selectHelpPage from './selectors';
import styled from 'styled-components';
import jumbotron from './jumbotron.jpg';

const Title = styled.h1`
  padding: .5rem;
  letter-spacing: .1rem;
  margin: 1rem 0;
  font-family: 'Jaapokki-Regular';
  text-transform: uppercase;

  @media (min-width: 768px) {
    width: 100%;
    position: absolute;
    top: 10rem;
    font-size: 2rem;
    color: white;
  }

  @media (min-width: 1440px) {
    font-size: 2.5rem;
    top: 15rem;
  }
`;

const Jumbotron = styled.div`
  width: 100%;
  height: 200px;
  background: url(${props => props.background}) center center;
  background-size: cover;

  @media (min-width: 768px) {
    height: 400px;
  }

  @media (min-width: 1440px) {
    height: 600px;
  }
`;

const Sub = styled.h3`
  margin: 0;
  padding: 2rem 3rem;
  color: #0097b0;
  font-family: 'Jaapokki-Regular';
  text-align: center;
`;

const Desc = styled.p`
  margin: 0;
  padding: 0 1.5rem 3rem 1.5rem;
`;

const Section = styled.div`
  padding: 3rem;
  background: #fafafa;
  text-align: center;
`;

const CallToAction = styled.button`
  padding: 1rem 2rem 0.75rem;
  letter-spacing: .3rem;
  border-bottom: 0.2rem solid #0097b0;
  font-size: 1.25rem;
  font-family: 'Jaapokki-Regular';
  text-transform: uppercase;

  &:hover {
    background: #0097b0;
    color: white;
  }
`;


export class HelpPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <Helmet
          title="HelpPage"
          meta={[
            { name: 'description', content: 'Description of HelpPage' },
          ]}
        />
        <Title h1 className="center">How we work.</Title>
        <Jumbotron background={jumbotron} />
        <div className="row">
          <Sub>
            Find out why it is painless and fast.
          </Sub>
          <Desc>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </Desc>
        </div>
        <Section calssName="row">
          <h4>Have a meeting?</h4>
          <CallToAction>Book a Room</CallToAction>
        </Section>
      </div>
    );
  }
}

const mapStateToProps = selectHelpPage();

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HelpPage);
