import expect from 'expect';
import helpPageReducer from '../reducer';
import { fromJS } from 'immutable';

describe('helpPageReducer', () => {
  it('returns the initial state', () => {
    expect(helpPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
