/*
 *
 * RoomModule reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  CHANGE_SELECTED,
  LOAD_ROOM_DONE,
  LOAD_ROOM_DETAIL_DONE,
} from './constants';
import DummyPict from './dum1.jpg';
import DummyPictTwo from 'containers/AboutPage/jumbotron.jpg';

const initialState = fromJS({
	selectedRoom: null,
	initialLoad: true,
	rooms: [],
});

function roomModuleReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case CHANGE_SELECTED:
    	return state.set('selectedRoom', action.payload);
    case LOAD_ROOM_DONE:
    	return state.set('rooms', action.payload);
    case LOAD_ROOM_DETAIL_DONE:
    	return state.set('selectedRoom', action.payload);
    default:
      return state;
  }
}

export default roomModuleReducer;
