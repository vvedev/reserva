import React from 'react';
import { connect } from 'react-redux';
import { selectGlobalJS } from 'containers/App/selectors';
import styled from 'styled-components';

const Dates = styled.div`
	margin-bottom: 1rem;
`;

const Day = styled.span`
	display: inline-block;
	display: none;

	@media (min-width: 768px) {
		width: 35%;
		display: inline-block;
		margin-left: 1rem;
		margin-right: .8rem;
		text-align: right;
	}
`;

const NextDay = styled.button`
	display: inline-block;
	float: left;
	width: 12%;
	margin: 0;
	height: 1.5rem;
	border-radius: 1rem;
	color: #333;

	&:disabled,
	&:hover,
	&:active {
		color: #FFF;
		background: #0097b0;
	}

	@media (min-width: 768px) {
		text-align: left;
	}
`;

const NowDay = styled.button`
	display: inline-block;
	float: left;
	width: 12%;
	margin: 0;
	height: 1.5rem;
	border-radius: 1rem;
	background: #AAA;
	color: #FFF;

	&:disabled,
	&:hover,
	&:active {
		background: #0097b0;
	}

	@media (min-width: 768px) {
		text-align: left;
	}
`;

const Yesterday = styled.button`
	display: inline-block;
	float: left;
	width: 12%;
	margin: 0;
	height: 1.5rem;
	border-radius: 1rem;
	color: #888;

	&:disabled,
	&:hover,
	&:active {
		color: #FFF;
		background: #0097b0;
	}

	@media (min-width: 768px) {
		text-align: left;
	}
`;

const Arrow = styled.button`
	display: inline-block;
	float: left;
	width: 8%;
	height: 1.5rem;
	border-radius: 1rem;
	margin: 0;
	padding; .5rem;
	background: #fba600;
	color: white;
`;

const Time = styled.div`
	padding: 1.25rem .5rem;
	height: 3.5rem;
	display: block;
	margin: 0;
`;

const Schedules = styled.div`
	min-height: 480px;
	margin-top: .5rem;
	padding-right: .5rem;
`;

const Row = styled.div`
	min-height: 40px;
`;

const Booking = styled.div`
	padding: .5rem;
	background: #FFF5E9;
	position: absolute;
	left: 0;
	right: 0;
	width: 100%;
`;

const BookingBackground = styled.div`
	border-top: 1px solid #f4f4f4;
	border-bottom: 1px solid #f4f4f4;
	height: 3.5rem;
`;

const Entry = styled.div`
	position: relative;
`;

const Month = styled.div`
	width: 100%;
	margin-top: 1rem;
	margin-bottom: 0.5rem;
	text-align: center;

	h1 {
		color: #0097b0;
		font-weight: 700;
		line-height: 1;
		margin: 0;
		text-transform: uppercase;
	}
`;

export class Schedule extends React.Component { // eslint-disable-line react/prefer-stateless-function
	static propTypes = {
		query: React.PropTypes.object,
		bookings: React.PropTypes.array,
	};

	constructor(props) {
		super(props);
		const now = new Date();
		let dateString = `${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`;

		if(this.props.query.date) {
			dateString = `${this.props.query.date.year()}-${this.props.query.date.month() + 1}-${this.props.query.date.date()}`;
		}

		const day = new Date(dateString);
		console.log(day.getDate());
		console.log(day.getDay());

		const sun = new Date(day.setDate(day.getDate() - day.getDay()));
		const mon = new Date(day.setDate(day.getDate() - day.getDay() + 1));
		const tue = new Date(day.setDate(day.getDate() - day.getDay() + 2));
		const wed = new Date(day.setDate(day.getDate() - day.getDay() + 3));
		const thu = new Date(day.setDate(day.getDate() - day.getDay() + 4));
		const fri = new Date(day.setDate(day.getDate() - day.getDay() + 5));
		const sat = new Date(day.setDate(day.getDate() - day.getDay() + 6));
		
		let tdy = new Date(dateString);
		let realtdy = new Date(`${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`);

		this.state = {
			today: realtdy,
			selected: tdy,
			day: {
				sun: sun,
				mon: mon,
				tue: tue,
				wed: wed,
				thu: thu,
				fri: fri,
				sat: sat,
			}
		};

		this.nextWeek = this.nextWeek.bind(this);
		this.lastWeek = this.lastWeek.bind(this);
		this.setDay = this.setDay.bind(this);
	}

	setDay(day) {
		this.setState({
			selected: day,
		});
	}

	nextWeek() {
		this.setState({
			day: {
				sun: new Date(this.state.day.sun.setDate(this.state.day.sun.getDate() + 7)),
				mon: new Date(this.state.day.mon.setDate(this.state.day.mon.getDate() + 7)),
				tue: new Date(this.state.day.tue.setDate(this.state.day.tue.getDate() + 7)),
				wed: new Date(this.state.day.wed.setDate(this.state.day.wed.getDate() + 7)),
				thu: new Date(this.state.day.thu.setDate(this.state.day.thu.getDate() + 7)),
				fri: new Date(this.state.day.fri.setDate(this.state.day.fri.getDate() + 7)),
				sat: new Date(this.state.day.sat.setDate(this.state.day.sat.getDate() + 7)),
			}
		});

		let selectedDay = this.state.day.sun;
		
		if(this.state.selected === 1) {
			selectedDay = this.state.day.mon;
		} else if(this.state.selected === 2) {
			selectedDay = this.state.day.tue;
		} else if(this.state.selected === 3) {
			selectedDay = this.state.day.wed;
		} else if(this.state.selected === 4) {
			selectedDay = this.state.day.thu;
		} else if(this.state.selected === 5) {
			selectedDay = this.state.day.fri;
		} else if(this.state.selected === 6) {
			selectedDay = this.state.day.sat;
		} 

		this.setDay(selectedDay);
	}

	lastWeek() {
		this.setState({
			day: {
				sun: new Date(this.state.day.sun.setDate(this.state.day.sun.getDate() - 7)),
				mon: new Date(this.state.day.mon.setDate(this.state.day.mon.getDate() - 7)),
				tue: new Date(this.state.day.tue.setDate(this.state.day.tue.getDate() - 7)),
				wed: new Date(this.state.day.wed.setDate(this.state.day.wed.getDate() - 7)),
				thu: new Date(this.state.day.thu.setDate(this.state.day.thu.getDate() - 7)),
				fri: new Date(this.state.day.fri.setDate(this.state.day.fri.getDate() - 7)),
				sat: new Date(this.state.day.sat.setDate(this.state.day.sat.getDate() - 7)),
			}
		});

		let selectedDay = this.state.day.sun;
		
		if(this.state.selected === 1) {
			selectedDay = this.state.day.mon;
		} else if(this.state.selected === 2) {
			selectedDay = this.state.day.tue;
		} else if(this.state.selected === 3) {
			selectedDay = this.state.day.wed;
		} else if(this.state.selected === 4) {
			selectedDay = this.state.day.thu;
		} else if(this.state.selected === 5) {
			selectedDay = this.state.day.fri;
		} else if(this.state.selected === 6) {
			selectedDay = this.state.day.sat;
		} 

		this.setDay(selectedDay);
	}

  convertToMinute(val) {
    var temp = val.split(":");
    var hour = parseInt(temp[0]);
    var minute = parseInt(temp[1]);
    return hour * 60 + minute;
  }

	render() {
		const SunElem = (this.state.today - this.state.day.sun) <= 0 ? ((this.state.today - this.state.day.sun) == 0 ? NowDay : NextDay) : Yesterday;
		const MonElem = (this.state.today - this.state.day.mon) <= 0 ? ((this.state.today - this.state.day.mon) == 0 ? NowDay : NextDay) : Yesterday;;
		const TueElem = (this.state.today - this.state.day.tue) <= 0 ? ((this.state.today - this.state.day.tue) == 0 ? NowDay : NextDay) : Yesterday;;
		const WedElem = (this.state.today - this.state.day.wed) <= 0 ? ((this.state.today - this.state.day.wed) == 0 ? NowDay : NextDay) : Yesterday;;
		const ThuElem = (this.state.today - this.state.day.thu) <= 0 ? ((this.state.today - this.state.day.thu) == 0 ? NowDay : NextDay) : Yesterday;;
		const FriElem = (this.state.today - this.state.day.fri) <= 0 ? ((this.state.today - this.state.day.fri) == 0 ? NowDay : NextDay) : Yesterday;;
		const SatElem = (this.state.today - this.state.day.sat) <= 0 ? ((this.state.today - this.state.day.sat) == 0 ? NowDay : NextDay) : Yesterday;;

	 	const monthNames = ["January", "February", "March", "April", "May", "June",
	  	"July", "August", "September", "October", "November", "December"
		];

		const bookingSchedule = this.props.bookings.map((value, key) => {
			const valueDate = new Date(value.date);
			const valueDuration = this.convertToMinute(value.end_time) - this.convertToMinute(value.start_time);
			const valueOffset = this.convertToMinute(value.start_time) - this.convertToMinute('08:00');

			if(valueDate.getFullYear() === this.state.selected.getFullYear() && valueDate.getMonth() === this.state.selected.getMonth() && valueDate.getDate() === this.state.selected.getDate()) {
				return (
					<Booking key={`booking-${value.id}`} style={{ top: `${(valueOffset / 60) * 3.5}rem`, height: `${(valueDuration / 60) * 3.5}rem` }}>
						<h4 className="no-gutter">{value.agenda}</h4>
						<p className="no-gutter italic">{value.user.name}</p>
					</Booking>
				);
			}
		});

		return (
			<div>
				<Month>
					<h1>{monthNames[this.state.selected.getMonth()]} {this.state.selected.getFullYear()}</h1>
				</Month>
				<Dates className="center">
					<Arrow onClick={this.lastWeek}>{`<`}</Arrow>
					<SunElem disabled={this.state.selected.getDay() === 0} onClick={() => this.setDay(this.state.day.sun)} ><Day>Sun</Day> {this.state.day.sun.getDate()}</SunElem>
					<MonElem disabled={this.state.selected.getDay() === 1} onClick={() => this.setDay(this.state.day.mon)} ><Day>Mon</Day> {this.state.day.mon.getDate()}</MonElem>
					<TueElem disabled={this.state.selected.getDay() === 2} onClick={() => this.setDay(this.state.day.tue)} ><Day>Tue</Day> {this.state.day.tue.getDate()}</TueElem>
					<WedElem disabled={this.state.selected.getDay() === 3} onClick={() => this.setDay(this.state.day.wed)} ><Day>Wed</Day> {this.state.day.wed.getDate()}</WedElem>
					<ThuElem disabled={this.state.selected.getDay() === 4} onClick={() => this.setDay(this.state.day.thu)} ><Day>Thu</Day> {this.state.day.thu.getDate()}</ThuElem>
					<FriElem disabled={this.state.selected.getDay() === 5} onClick={() => this.setDay(this.state.day.fri)} ><Day>Fri</Day> {this.state.day.fri.getDate()}</FriElem>
					<SatElem disabled={this.state.selected.getDay() === 6} onClick={() => this.setDay(this.state.day.sat)} ><Day>Sat</Day> {this.state.day.sat.getDate()}</SatElem>
					<Arrow onClick={this.nextWeek}>{`>`}</Arrow>
				</Dates>
				<Schedules className="small-12 columns">
					<div className="small-3 medium-2 large-1 columns center">
						<Time>08.00</Time>
						<Time>09.00</Time>
						<Time>10.00</Time>
						<Time>11.00</Time>
						<Time>12.00</Time>
						<Time>13.00</Time>
						<Time>14.00</Time>
						<Time>15.00</Time>
						<Time>16.00</Time>
						<Time>17.00</Time>
						<Time>18.00</Time>
						<Time>19.00</Time>
						<Time>20.00</Time>
						<Time>21.00</Time>
					</div>
					<Entry className="small-9 medium-10 large-11 columns">
						<div className="row">
							<BookingBackground />
							<BookingBackground />
							<BookingBackground />
							<BookingBackground />
							<BookingBackground />
							<BookingBackground />
							<BookingBackground />
							<BookingBackground />
							<BookingBackground />
							<BookingBackground />
							<BookingBackground />
							<BookingBackground />
							<BookingBackground />
							<BookingBackground />
						</div>
						{ bookingSchedule }
					</Entry>
					<div className="small-9 medium-10 large-11 columns">
						<Row className="row"/>
					</div>
				</Schedules>
			</div>
		);
	}
}

const mapStateToProps = selectGlobalJS();

export default connect(mapStateToProps, null)(Schedule);