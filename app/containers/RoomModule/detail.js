import React from 'react';
import styled from 'styled-components';
import image from './dum1.jpg';
import Schedule from './schedule';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

const Title = styled.h1`
  letter-spacing: .1rem;
  text-align: center;

  @media (min-width: 768px) {
    font-size: 2rem;
  }
`;

const Column = styled.div`
	padding: 0;
`;

const Meta = styled.h5`
	padding: .6rem 1.5rem;
	height: 2.5rem;
	text-align: right;
  font-family: 'Jaapokki-Regular';
  text-transform: uppercase;
  background: #AAA;
  color: #FFF;

	@media (min-width: 768px) {
		font-size: 1.1rem;
		text-align: center;
	}
`;

const Status = styled.div`
	padding: .6rem 1.5rem;
	height: 2.5rem;
	background: #f8f8f8;
	text-align: right;
  font-family: 'Jaapokki-Regular';
  text-transform: uppercase;

	@media (min-width: 768px) {
		margin-bottom: 1rem;
		font-size: 1.1rem;
	}
`;

const CallToAction = styled.button`
	height: 2.5rem;
	width: 100%;
	padding: .6rem .8rem;
	letter-spacing: .1rem;
  border: 0.2rem solid #283739;
  font-family: 'Jaapokki-Regular';

	&:hover {
		background: #283739;
		color: white;
	}

	@media (max-width: 768px) {
		height: 5rem;
	}
`;

const Preview = styled.img`
	width: 100%;
	max-height: 75vh;
	object-fit: cover;
	padding: 0 0 2rem;
`;

export default class Detail extends React.Component { // eslint-disable-line react/prefer-stateless-function
	static propTypes = {
		global: React.PropTypes.object,
		push: React.PropTypes.func,
		selectedRoom: React.PropTypes.object,
		rooms: React.PropTypes.array,
		loadRoomDetail: React.PropTypes.func,
	};

	constructor(props) {
		super(props);
		this.go = this.go.bind(this);
	}


  componentDidMount() {
    this.props.loadRoomDetail();
  }

	go(url) {
		this.props.push(url);
	}

	render() {
		let content = (<h1>Room doesn't exist</h1>);
		if(this.props.selectedRoom) {
			content = (
				<div className="row">
					<Preview src={this.props.selectedRoom.image} alt="preview-image" />
					<Title className="uppercase">{this.props.selectedRoom.name}</Title>
					<Column className="small-8 columns">
						<Meta>
							<span>{this.props.selectedRoom.location}</span>
						</Meta>
					</Column>
					<Column className="small-4 columns center">
						<CallToAction className="uppercase"  onClick={() => this.go('/booking/create')}>
							Book This Room
						</CallToAction>
					</Column>
					<Column className="small-12 columns">
						<Schedule bookings={this.props.selectedRoom.bookings} />
					</Column>
				</div>
			);
		}
		return (
			<div>
				{content}
			</div>
		);
	}
}
