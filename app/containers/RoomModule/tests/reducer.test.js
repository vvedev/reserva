import expect from 'expect';
import roomModuleReducer from '../reducer';
import { fromJS } from 'immutable';

describe('roomModuleReducer', () => {
  it('returns the initial state', () => {
    expect(roomModuleReducer(undefined, {})).toEqual(fromJS({}));
  });
});
