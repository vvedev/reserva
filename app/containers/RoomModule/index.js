/*
 *
 * RoomModule
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import selectRoomModule from './selectors';
import { push } from 'react-router-redux';
import Rooms from './rooms';
import Detail from './detail';
import { loading } from 'containers/App/actions';
import { loadRoom, loadRoomDetail } from './actions';

export class RoomModule extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    params: React.PropTypes.object,
    rooms: React.PropTypes.array,
    push: React.PropTypes.func,
    loadRoom: React.PropTypes.func,
    loadRoomDetail: React.PropTypes.func,
    selectedRoom: React.PropTypes.object,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.loading();
  }

  render() {
    let content = (
      <Rooms push={this.props.push} rooms={this.props.rooms} research={this.props.loadRoom} loadRoom={this.props.loadRoom} />
    );

    if(this.props.params.slug) {
      content = (
        <Detail push={this.props.push} selectedRoom={this.props.selectedRoom} rooms={this.props.rooms} loadRoomDetail={this.props.loadRoomDetail} />
      );
    }
    
    return (
      <div>
        <Helmet
          title="Rooms"
          meta={[
            { name: 'description', content: 'Description of RoomModule' },
          ]}
        />
        {content}
      </div>
    );
  }
}

const mapStateToProps = selectRoomModule();

function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
    loadRoom: () => dispatch(loadRoom()),
    loadRoomDetail: () => dispatch(loadRoomDetail()),
    loading: () => dispatch(loading()),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomModule);
