import { take, call, put, select, cancel, fork } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import moment from 'moment';
import request from 'utils/request';

import { selectGlobalJS } from 'containers/App/selectors';
import { loading, loadingDone } from 'containers/App/actions';
import { API_ROOM_URL } from 'containers/App/api';
import { LOAD_ROOM, LOAD_ROOM_DETAIL } from './constants';
import { loadRoomDone, loadRoomDetailDone } from './actions';

import DummyPict from './dum1.jpg';
import DummyPictTwo from 'containers/AboutPage/jumbotron.jpg';

export function* fetchResult() {
	yield put(loading());
	
	const globalState = yield select(selectGlobalJS());
	const dateQuery = globalState.query.date;
	const startHourQuery = globalState.query.startHour;
	const endHourQuery = globalState.query.endHour;

	//build query for search
	let day = '';
	
	if(dateQuery) {
		day = `${dateQuery.year()}-${dateQuery.month() + 1}-${dateQuery.date()}`;
	}

	let start = '';
	
	if(startHourQuery) {
		start = `${startHourQuery.hour()}-${startHourQuery.minute()}`;
	}

	let end = '';
	
	if(endHourQuery) {
		end = `${endHourQuery.hour()}-${endHourQuery.minute()}`;
	}

	const today = new moment();
	let searchQuery = '';

	if (day !== '') {
		if(start !== '') {
			if(end !== '') { 
				searchQuery = `?date=${day}&start_time=${start}&end_time=${end}`;
			} else {
				searchQuery = `?date=${day}&start_time=${start}&end_time=23-59`;
			}
		} else {
			if(end !== '') {
				searchQuery = `?date=${day}&start_time=00-00&end_time=${end}`;
			} else {
				searchQuery = `?date=${day}&start_time=00-00&end_time=23-59`;
			}
		}
	} else if (start !== '') {
		if (end !== '') {
			searchQuery = `?date=${today.year()}-${today.month()}-${today.date()}&start_time=${start}&end_time=${end}`;
		} else {
			searchQuery = `?date=${today.year()}-${today.month()}-${today.date()}&start_time=${start}&end_time=23-59`;
		}
	} else if (end !== '') {
		searchQuery = `?date=${today.year()}-${today.month()}-${today.date()}&start_time=00-00&end_time=${end}`;
	}

	const requestURL = `${API_ROOM_URL}${searchQuery}`;

	const fetchResultCall = yield call(request, requestURL, {
    method: 'GET',
    headers: {
      'Accept': 'application/json'
    }
  });

  if (!fetchResultCall.err) {
		yield put(loadRoomDone(fetchResultCall.data));
  } else {
  	console.log(fetchResultCall.err);
  }

	yield put(loadingDone());
}

export function* resultWatcher() {
	while (yield take(LOAD_ROOM)) {
		yield call(fetchResult);
	}
}

export function* resultSaga() {
	const resultLoader = yield fork(resultWatcher);

	yield take(LOCATION_CHANGE);
	yield cancel(resultLoader);
}

export function* fetchDetail() {
	yield put(loading());
	
	const roomId = window.location.pathname.replace('/room/','');

	const requestURL = `${API_ROOM_URL}${roomId}`;

	const fetchDetailCall = yield call(request, requestURL, {
    method: 'GET',
    headers: {
      'Accept': 'application/json'
    }
  });

  if (!fetchDetailCall.err) {
		yield put(loadRoomDetailDone(fetchDetailCall.data));
  } else {
  	console.log(fetchDetailCall.err);
  }

	yield put(loadingDone());
}

export function* detailWatcher() {
	while (yield take(LOAD_ROOM_DETAIL)) {
		yield call(fetchDetail);
	}
}


export function* detailSaga() {
	const detailLoader = yield fork(detailWatcher);

	yield take(LOCATION_CHANGE);
	yield cancel(detailLoader);
	
}

// Individual exports for testing
export function* roomSaga() {
	yield [
		fork(resultSaga),
		fork(detailSaga),
	];
}

// All sagas to be loaded
export default [
  roomSaga,
];
