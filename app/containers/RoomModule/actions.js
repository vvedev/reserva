/*
 *
 * RoomModule actions
 *
 */

import {
  DEFAULT_ACTION,
  CHANGE_SELECTED,
  LOAD_ROOM,
  LOAD_ROOM_DONE,
  LOAD_ROOM_DETAIL,
  LOAD_ROOM_DETAIL_DONE,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function changeSelected(payload) {
  return {
    type: CHANGE_SELECTED,
    payload,
  };
}

export function loadRoom() {
  return {
    type: LOAD_ROOM,
  };
}

export function loadRoomDone(payload) {
  return {
    type: LOAD_ROOM_DONE,
    payload,
  };
}


export function loadRoomDetail() {
  return {
    type: LOAD_ROOM_DETAIL,
  };
}

export function loadRoomDetailDone(payload) {
  return {
    type: LOAD_ROOM_DETAIL_DONE,
    payload,
  };
}
