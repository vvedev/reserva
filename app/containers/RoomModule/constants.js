/*
 *
 * RoomModule constants
 *
 */

export const DEFAULT_ACTION = 'app/RoomModule/DEFAULT_ACTION';
export const LOAD_ROOM = 'app/RoomModule/LOAD_ROOM_DONE';
export const LOAD_ROOM_DONE = 'app/RoomModule/LOAD_ROOM_DONE';
export const LOAD_ROOM_DETAIL = 'app/RoomModule/LOAD_ROOM_DETAIL';
export const LOAD_ROOM_DETAIL_DONE = 'app/RoomModule/LOAD_ROOM_DETAIL_DONE';
export const CHANGE_SELECTED = 'app/RoomModule/CHANGE_SELECTED';
