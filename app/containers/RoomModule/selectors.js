import { createSelector } from 'reselect';

/**
 * Direct selector to the roomModule state domain
 */
const selectRoomModuleDomain = () => (state) => state.get('roomModule');

/**
 * Other specific selectors
 */


/**
 * Default selector used by RoomModule
 */

const selectRoomModule = () => createSelector(
  selectRoomModuleDomain(),
  (substate) => substate.toJS()
);

export default selectRoomModule;
export {
  selectRoomModuleDomain,
};
