import React from 'react';
import styled from 'styled-components';
import SearchModule from 'containers/SearchModule';
import { isEmpty } from 'lodash';
import Preview from './dum1.jpg';

const Room = styled.div`
	background: #EEE;
	min-height: calc(100vh - 3rem);
	position: relative;
	padding: 2rem 0;
`;

const Content = styled.div`
	margin: 0 auto;
	max-width: 1200px;
`;

const Title = styled.h1`
  letter-spacing: .1rem;
  line-height: 1;
  margin: 0 0 1rem;
  font-family: 'Jaapokki-Regular';

  @media (min-width: 768px) {
    font-size: 2rem;
    padding: 0 2.5rem;
  }
`;

const Excerpt = styled.div`
	margin: 1rem;
	width: calc(100% - 2rem);
	background: #FFF;
	text-align: left;
`;

const ExcerptContent = styled.div`
	padding: 2rem;
	position: relative;
`;

const Name = styled.h2`
	margin: 0;
	line-height: 1;

	@media (min-width: 768px) {
		font-size: 1.3rem;
	}
`;

const Location = styled.h5`
	margin: 0;
	line-height: 1;

	@media (min-width: 768px) {
		font-size: 1.1rem;
	}
`;

const Desc = styled.p`
	margin: 0;

	@media (min-width: 768px) {
		font-size: 1rem;
	}
`;

const Status = styled.button`
	font-style: italic;
	color: #0097b0;
	padding: 0.5rem 1rem;
	border-bottom: 0.1rem solid #0097b0;
	position: absolute;
	top: 1.5rem;
	right: 1rem;

	&:disabled {
		color: #AAA;
		border-bottom: 0.1rem solid #AAA;
	}

	@media (min-width: 768px) {
		font-size: 1.1rem;
	}
`;

const Prev = styled.img`
	width: 100%;
	height: 15rem;
	object-fit: cover;
	object-location: center;
`;

const Search = styled.div`
	padding: 1rem;

	@media screen and (min-width: 1200px) {
		padding: 1rem 0;
	}
`;

const SearchButton = styled.button`
  padding: 1rem 2rem 0.75rem;
  width: 100%;
  letter-spacing: .3rem;
  text-transform: uppercase;
  font-size: 1rem;
  background: #0097b0;
  color: #FFF;
  font-family: 'Jaapokki-Regular';

  &:hover {
    background: #EEEEEE;
    color: #0097b0;
  }
`;

export default class Rooms extends React.Component { // eslint-disable-line react/prefer-stateless-function
	static propTypes = {
		push: React.PropTypes.func,
		rooms: React.PropTypes.array,
		research: React.PropTypes.func,
		loadRoom: React.PropTypes.func,
	};

  componentDidMount() {
    this.props.loadRoom();
  }

	render() {
		let roomElement = (<p>No Room Available</p>);

		if (this.props.rooms) {
			roomElement = this.props.rooms.map((value, key) => {
				return (
							<Excerpt key={`room-${key}`} className="small-12 columns">
								<div className="row expanded">
									<div className="small-12 medium-4 columns">
										<Prev src={value.image} alt="room picture" />
									</div>
									<ExcerptContent className="small-12 medium-8 columns">
										<Status onClick={() => this.props.push(`/room/${value.id}`)}>
											Available <span className="flaticon-arrows" />
										</Status>
										<Name>{value.name}</Name>
										<Location>
											<span>{value.location}</span>
										</Location>
									</ExcerptContent>
								</div>
							</Excerpt>
				);
			});
		}

		return (
			<div className="row expanded">
				<div className="small-12 columns">
					<Content>
						<div className="row">
							<Search className="small-12 columns">
								<SearchModule />
								<SearchButton onClick={this.props.research} >Search Again</SearchButton>
							</Search>
						</div>
					</Content>
				</div>
				<Room className="small-12 columns">
					<Content>
						<Title className="center uppercase">Available Rooms</Title>
						<div className="row">
							{roomElement}
						</div>
					</Content>
				</Room>
			</div>
		);
	}
}
