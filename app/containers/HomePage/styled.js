import styled from 'styled-components';

export const Fields = styled.div`
  width: 100%;
  position: absolute;
  top: 50%;
  left: 0;
  right: 0;
  transform: translateY(-50%);

  @media screen and (max-width: 64rem) {
    width: 90%;
    left: 5%;
    right: 5%;
  }
`;

export const Container = styled.div`
  position: relative;
  width: 100%;
  height: calc(100vh - 3rem);
`;

export const SubTitle = styled.h1`
  margin: 0 0 1rem;
  line-height: 1;
  font-size: 10rem;
  color: #0097b0;
  font-family: 'Jaapokki-Regular';
  text-transform: uppercase;
  text-align: left;
`;


export const CallToAction = styled.button`
  padding: 1rem 2rem 0.75rem;
  width: 100%;
  letter-spacing: .3rem;
  text-transform: uppercase;
  font-size: 1rem;
  background: #0097b0;
  color: #FFF;
  font-family: 'Jaapokki-Regular';

  &:hover {
    background: #EEEEEE;
    color: #0097b0;
  }
`;