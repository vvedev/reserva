/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import jumbotron from './jumbotron.jpg';
import Helmet from 'react-helmet';
import SearchModule from 'containers/SearchModule';
import { Fields, Container, SubTitle, CallToAction } from './styled';


export class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    push: React.PropTypes.func,
  };

  render() {
    return (
      <div>
        <Helmet
          title="Home - Reserva"
          meta={[
            { name: 'description', content: 'Reserva Homepage' },
          ]}
        />
        <div className="row">
          <div className="small-12 columns">
            <Container>
              <Fields>
                <SubTitle>
                  <FormattedMessage {...messages.header} />
                </SubTitle>
                <SearchModule />
                <div className="center">
                  <CallToAction onClick={() => this.props.push('/result')}>Search</CallToAction>
                </div>
              </Fields>
            </Container>
          </div>
        </div>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
  };
}

export default connect(null, mapDispatchToProps)(HomePage);
