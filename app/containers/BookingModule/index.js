/*
 *
 * BookingModule
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import selectBookingModule from './selectors';
import Booking from './booking';
import Detail from './detail';
import Form from './form';
import styled from 'styled-components';

export class BookingModule extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    params: React.PropTypes.object,
  };

  render() {
    let content = (
      <Detail />
    );

    if (this.props.params.slug.startsWith('index')) {
      content = (
        <Booking />
      );
    } else if (this.props.params.slug.startsWith('create')) {
      content = (
        <Form />
      );
    }

    return (
      <div>
        <Helmet
          title="Bookings"
          meta={[
            { name: 'description', content: 'Description of BookingModule' },
          ]}
        />
        {content}
      </div>
    );
  }
}

const mapStateToProps = selectBookingModule();

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BookingModule);
