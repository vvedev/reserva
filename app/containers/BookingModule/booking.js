import React from 'react';
import styled from 'styled-components';

const Title = styled.h1`
  letter-spacing: .1rem;
  margin: 1rem 0;
  font-family: 'Jaapokki-Regular';
  text-transform: uppercase;

  @media (min-width: 768px) {
    font-size: 2rem;
    padding: 0 2.5rem;
  }
`;

const Pending = styled.div`
	border-left: 5px solid #fba600;
	padding: .5rem;
	margin-bottom: 1.5rem;
	background: #fdfdfd;
`;

const Approved = styled.div`
	border-left: 5px solid #1abc9c;
	padding: .5rem;
	margin-bottom: 1.5rem;
	background: #fdfdfd;
`;

const Rejected = styled.div`
	border-left: 5px solid #DC5539;
	padding: .5rem;
	margin-bottom: 1.5rem;
	background: #f8f8f8;
`;

const Canceled = styled.div`
	border-left: 5px solid #666;
	padding: .5rem;
	margin-bottom: 1.5rem;
	background: #f8f8f8;
`;

const Column = styled.div`
	padding: .5rem;
`;

const Agenda = styled.h2`
	@media (min-width: 768px) {
		font-size: 1.3rem;
	}
`;

const Meta = styled.h5`
	@media (min-width: 768px) {
		font-size: 1.1rem;
	}
`;

const Desc = styled.p`
	margin: 0;

	@media (min-width: 768px) {
		font-size: 1rem;
	}
`;

const Status = styled.div`
	font-style: italic;
	font-weight: bold;

	@media (min-width: 768px) {
		font-size: 1.1rem;
	}
`;

export default class Booking extends React.Component { // eslint-disable-line react/prefer-stateless-function
	render() {
		return (
			<div>
				<Title className="center uppercase">Create Booking</Title>
				<Pending className="row">
					<div className="small-12 columns">
						<Column className="small-6 columns">
							<Agenda>
								Weekly Meeting
							</Agenda>
							<Meta>
								<span>11 November 2016, 10:10 - 11 November 2016, 12:10</span>
							</Meta>
							<Status>
								<span className="yellow">Pending</span>
							</Status>
						</Column>
						<Column className="small-6 columns">
							<Desc>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua.
							</Desc>
						</Column>
					</div>
				</Pending>
				<Approved className="row">
					<div className="small-12 columns">
						<Column className="small-6 columns">
							<Agenda>
								Weekly Meeting
							</Agenda>
							<Meta>
								<span>11 November 2016, 10:10 - 11 November 2016, 12:10</span>
							</Meta>
							<Status>
								<span className="green">Approved</span>
							</Status>
						</Column>
						<Column className="small-6 columns">
							<Desc>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua.
							</Desc>
						</Column>
					</div>
				</Approved>
				<Rejected className="row">
					<div className="small-12 columns">
						<Column className="small-6 columns">
							<Agenda className="grey">
								Weekly Meeting
							</Agenda>
							<Meta>
								<span className="grey">11 November 2016, 10:10 - 11 November 2016, 12:10</span>
							</Meta>
							<Status>
								<span className="red">Rejected</span>
							</Status>
						</Column>
						<Column className="small-6 columns">
							<Desc className="grey">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua.
							</Desc>
						</Column>
					</div>
				</Rejected>
				<Canceled className="row">
					<div className="small-12 columns">
						<Column className="small-6 columns">
							<Agenda className="grey">
								Weekly Meeting
							</Agenda>
							<Meta>
								<span className="grey">11 November 2016, 10:10 - 11 November 2016, 12:10</span>
							</Meta>
							<Status>
								<span className="grey">Canceled</span>
							</Status>
						</Column>
						<Column className="small-6 columns">
							<Desc className="grey">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua.
							</Desc>
						</Column>
					</div>
				</Canceled>
			</div>
		);
	}
}
