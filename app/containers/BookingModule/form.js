import React from 'react';
import styled from 'styled-components';
import DatePicker from 'react-datepicker';
import TimePicker from 'rc-time-picker';
import Moment from 'moment';

const Title = styled.h1`
  letter-spacing: .1rem;
  margin: 1rem 0;
  font-family: 'Jaapokki-Regular';

  @media (min-width: 768px) {
    font-size: 2rem;
    padding: 0 2.5rem;
  }
`;

const Fields = styled.form`
	width: 90%;
	margin: 0 auto;
`;

const Input = styled.input`
	padding: 1rem;
	margin-bottom: 1rem;
	text-align: left;
	background: #fdfdfd;
	border-bottom: .2rem solid #0097b0;
`;

const Textarea = styled.textarea`
	padding: 1rem;
	margin-bottom: 1rem;
	text-align: left;
	background: #fdfdfd;
	border-bottom: .2rem solid #0097b0;
	color: #666;
`;

const DateIcon = styled.span`
  margin: 0;
  line-height: 1;
  background: #0097b0;
  color: #eee;
  font-size: 1rem;
  padding: 0.9rem 0.5rem;
  border-bottom: 0.2rem solid #0097b0;
`;

const ClockIcon = styled.span`
  margin: 0;
  line-height: 1;
  background: #0097b0;
  color: #eee;
  font-size: 1rem;
  padding: 0.9rem 0.5rem;
  border-bottom: 0.2rem solid #0097b0;

  @media (min-width: 768px) {
    margin-left: 1rem;
  }
`;

const ToIcon = styled.span`
  margin: 0;
  line-height: 1;
  background: #0097b0;
  color: #eee;
  font-size: 1rem;
  padding: 0.9rem 0.5rem;
  border-bottom: 0.2rem solid #0097b0;
`;

const CallToAction = styled.button`
  padding: 1rem 2rem 0.75rem;
  margin-top: 2rem;
  letter-spacing: .75rem;
  border-bottom: 0.2rem solid #0097b0;
  text-transform: uppercase;
  font-size: 1.5rem;
  font-family: 'Jaapokki-Regular';

  &:hover {
    background: #0097b0;
    color: white;
  }
`;
export default class Form extends React.Component { // eslint-disable-line react/prefer-stateless-function
	constructor(props) {
		super(props);

		this.state = {
			date: null,
			startHour: null,
			endHour: null,
		}

		this.handleDate = this.handleDate.bind(this);
	}

	handleDate(date) {
		this.setState({ date });
	}

	handeStartHour(startHour) {
	    this.setState({ startHour });
	}

	handeEndHour(endHour) {
	    this.setState({ endHour });
	}
	render() {
		return(
			<div>
				<Title className="center uppercase">Create Booking</Title>
				<div className="row">
				<Fields>
					<Input type="text" placeholder="Agenda" className="small-12 medium-12 columns" />
					<Textarea type="text" rows="5" className="small-12 medium-12 columns">Description</Textarea>
					<DateIcon className="flaticon-calendar" />
					<DatePicker
						placeholderText="Date" 
						dateFormat="DD/MM/YYYY"
						selected={this.state.date}
						onChange={this.handleDate}
					/>
					<ClockIcon className="flaticon-clock" />
					<TimePicker
						placeholder="Start Time"
						defaultValue={this.state.startHour}
						onChange={this.handeStartHour}
						showSecond={false}
					/>
					<ToIcon className="flaticon-arrows" />
					<TimePicker
						placeholder="End Time"
						defaultValue={this.state.endHour}
						onChange={this.handleEndHour}
						showSecond={false}
					/>
					<div className="center">
						<CallToAction>Book</CallToAction>
					</div>
				</Fields>
				</div>
			</div>
		);
	}
}