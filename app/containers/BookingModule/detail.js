import React from 'react';
import styled from 'styled-components';
import image from './dum1.jpg';

const Title = styled.h1`
  letter-spacing: .1rem;
  margin: 1rem 0;

  @media (min-width: 768px) {
    font-size: 2rem;
    padding: 0 2.5rem;
  }
`;

const Column = styled.div`
	padding: .5rem;
`;

const Meta = styled.h5`
	@media (min-width: 768px) {
		font-size: 1.1rem;
	}
`;

const Desc = styled.p`
	margin: 0;

	@media (min-width: 768px) {
		font-size: 1rem;
	}
`;

const Status = styled.div`
	padding: .5rem;
	margin-bottom: 1rem;
	background: #f8f8f8;
	font-style: italic;
	font-weight: bold;

	@media (min-width: 768px) {
		font-size: 1.1rem;
	}
`;

const Upload = styled.button`
	padding: .8rem;
	margin: 0 1rem 1rem 1rem;
	letter-spacing: .1rem;
	border: 1px solid #1abc9c;

	&:hover {
		background: #1abc9c;
		color: white;
	}
`;

const Proof = styled.img`
	width: 100%;
	padding: 1rem 2rem 2rem 2rem;
`;

export default class Booking extends React.Component { // eslint-disable-line react/prefer-stateless-function
	render() {
		return (
			<div>
				<Title className="center uppercase">Weekly Meeting</Title>
				<div className="row">
					<div className="small-12 columns">
						<Status className="center">
							<span className="green">Available</span>
						</Status>
						<Column className="small-6 columns right">
							<Meta>
								<span className="blue">Kenny Reida Dharmawan
								<br />
								11 November 2016, 10:10 - 11 November 2016, 12:10</span>
							</Meta>
						</Column>
						<Column className="small-6 columns">
							<Desc>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua.
							</Desc>
						</Column>
						<Column className="small-12 columns center">
							<Upload className="uppercase">Upload Proof</Upload>
							<Proof src={image} alt="proof-image" />
						</Column>
					</div>
				</div>
			</div>
		);
	}
}
