import { createSelector } from 'reselect';

/**
 * Direct selector to the bookingModule state domain
 */
const selectBookingModuleDomain = () => (state) => state.get('bookingModule');

/**
 * Other specific selectors
 */


/**
 * Default selector used by BookingModule
 */

const selectBookingModule = () => createSelector(
  selectBookingModuleDomain(),
  (substate) => substate.toJS()
);

export default selectBookingModule;
export {
  selectBookingModuleDomain,
};
