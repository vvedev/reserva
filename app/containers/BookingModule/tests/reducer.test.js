import expect from 'expect';
import bookingModuleReducer from '../reducer';
import { fromJS } from 'immutable';

describe('bookingModuleReducer', () => {
  it('returns the initial state', () => {
    expect(bookingModuleReducer(undefined, {})).toEqual(fromJS({}));
  });
});
