/*
 *
 * App constants
 *
 */

export const DEFAULT_ACTION = 'app/App/DEFAULT_ACTION';
export const LOADING = 'app/App/LOADING';
export const LOADING_DONE = 'app/App/LOADING_DONE';
export const CLOSE_MENU = 'app/App/CLOSE_MENU';
export const OPEN_MENU = 'app/App/OPEN_MENU';
export const CHANGE_QUERY = 'app/App/CHANGE_QUERY';
