/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { 
  DEFAULT_ACTION,
  OPEN_MENU,
  CLOSE_MENU,
  CHANGE_QUERY,
  LOADING,
  LOADING_DONE,
} from './constants';

import { fromJS } from 'immutable';

// The initial state of the App
const initialState = fromJS({
  loading: false,
  error: false,
  menu: false,
  role: '',
  userData: {},
  query: {
    date: null,
    startHour: null,
    endHour: null,
  },
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case OPEN_MENU:
      return state.set('menu', true);
    case CLOSE_MENU:
      return state.set('menu', false);
    case LOADING:
      return state.set('loading', true);
    case LOADING_DONE:
      return state.set('loading', false);
    case CHANGE_QUERY:
      return state.setIn(['query', action.field], action.value);
    default:
      return state;
  }
}

export default appReducer;
