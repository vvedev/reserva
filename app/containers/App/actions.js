/*
 *
 * App actions
 *
 */

import {
  DEFAULT_ACTION,
  CLOSE_MENU,
  OPEN_MENU,
  CHANGE_QUERY,
  LOADING,
  LOADING_DONE,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function closeMenu() {
  return {
    type: CLOSE_MENU,
  };
}

export function openMenu() {
  return {
    type: OPEN_MENU,
  };
}

export function loading() {
  return {
    type: LOADING,
  };
}

export function loadingDone() {
  return {
    type: LOADING_DONE,
  };
}

export function changeQuery(field, value) {
  return {
    type: CHANGE_QUERY,
    field,
    value,
  };
}
