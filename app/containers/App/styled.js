import styled from 'styled-components';

export const ContentContainer = styled.div`
  min-height: calc(100vh - 2.5rem);
  position: relative;
`;

export const Loading = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 2000;
  background: #FFF;
  display: ${props => props.visibility ? 'block' : 'none'};
`;

export const LoadingContent = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`;

export const Centralizer = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const LoadingMessage = styled.h1`
  margin: 1rem 0 0;
  font-size: 1.5rem;
  font-family: 'Jaapokki-Enchance';
  text-transform: uppercase;
  color: #00ADB5;
`;
