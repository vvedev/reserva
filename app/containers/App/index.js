/**
 *
 * App.react.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { connect } from 'react-redux';
import { selectGlobalJS } from './selectors';
import Header from 'components/Header';
import Footer from 'components/Footer';
import OffscreenMenu from 'containers/OffscreenMenu';
import { Loading, LoadingContent, LoadingMessage, Centralizer, ContentContainer } from './styled';

export class App extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    children: React.PropTypes.node,
    loading: React.PropTypes.bool,
  };

  render() {
    return (
      <div>
        <Loading visibility={this.props.loading}>
          <LoadingContent>
            <Centralizer>
              <div className="sk-folding-cube">
                <div className="sk-cube1 sk-cube"></div>
                <div className="sk-cube2 sk-cube"></div>
                <div className="sk-cube4 sk-cube"></div>
                <div className="sk-cube3 sk-cube"></div>
              </div>
              <LoadingMessage>RESERVA</LoadingMessage>
            </Centralizer>
          </LoadingContent>
        </Loading>
        <Header />
        <OffscreenMenu />
        <ContentContainer>
          {React.Children.toArray(this.props.children)}
        </ContentContainer> 
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = selectGlobalJS();

export default connect(mapStateToProps, null)(App);
