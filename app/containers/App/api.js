/*
 *
 * Reserva API links
 *
 */

export const API_LOGIN_URL = 'https://private-515d5-roomseekme.apiary-mock.com/oauth/token';
export const API_REGISTER_URL = 'https://private-515d5-roomseekme.apiary-mock.com/register';
export const API_ROOM_URL = 'https://private-515d5-roomseekme.apiary-mock.com/rooms/';
export const API_BOOKING_URL = 'https://private-515d5-roomseekme.apiary-mock.com/bookings/';
export const API_USER_URL = 'https://private-515d5-roomseekme.apiary-mock.com/users/';
