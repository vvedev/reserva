# routing
/                   landing -> search field
/room/id            room detail
/bookings           list of bookings
/about              about reserva
/help               how to use reserva

# color
red                 #DC5539
blue                #00adca
yellow              #fba600a